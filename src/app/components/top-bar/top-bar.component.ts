import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ColorSchemeService } from 'src/app/_services/color-scheme.service';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
})
export class TopBarComponent implements OnInit {
  title: string;
  year: string;

  isDark: boolean = this.colorSchemeService.isDark.value;

  @Output() $search: EventEmitter<{
    title: string;
    year: string;
  }> = new EventEmitter();

  constructor(private colorSchemeService: ColorSchemeService) {}

  ngOnInit(): void {}

  handleSearch() {
    this.$search.emit({
      title: this.title,
      year: this.year,
    });
  }

  handleTheme() {
    this.colorSchemeService.isDark.next(!this.colorSchemeService.isDark.value);
    this.isDark = this.colorSchemeService.isDark.value;
  }
}
