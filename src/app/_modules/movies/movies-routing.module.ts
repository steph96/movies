import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewMovieComponent } from 'src/app/pages/view-movie/view-movie.component';

const routes: Routes = [{ path: '', component: ViewMovieComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesRoutingModule {}
