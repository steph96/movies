import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';

import { MoviesRoutingModule } from './movies-routing.module';
import { ViewMovieComponent } from 'src/app/pages/view-movie/view-movie.component';

@NgModule({
  declarations: [ViewMovieComponent],
  imports: [CommonModule, MoviesRoutingModule, MatIconModule],
})
export class MoviesModule {}
