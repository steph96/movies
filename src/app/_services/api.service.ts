import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private httpClient: HttpClient) {}

  public getMovies(searchString?: string, year?: string) {
    return this.httpClient.get(
      `http://www.omdbapi.com/?t=${searchString}&y=${year}&type="movie"&apikey=cc070fcf`
    );
  }
  public getMovieById(id: string) {
    return this.httpClient.get(
      `http://www.omdbapi.com/?i=${id}&type="movie"&apikey=cc070fcf`
    );
  }
}
