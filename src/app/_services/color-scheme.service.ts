import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColorSchemeService {
  public isDark: BehaviorSubject<boolean> = new BehaviorSubject(true);
  constructor() { }
}
