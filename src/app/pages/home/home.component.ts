import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApiService } from 'src/app/_services/api.service';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ColorSchemeService } from 'src/app/_services/color-scheme.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  public $destroyed: Subject<void> = new Subject();

  filteredMovies = [];
  isDark: boolean;
  isSearching: boolean = false;

  year: string = '';
  title: string = '';

  alphanumeric: string = 'abcdefghijklmnopqrstuvwxyz0123456789';
  searchLetters = [];

  constructor(
    private apiService: ApiService,
    private router: Router,
    private colorSchemeService: ColorSchemeService
  ) {}

  ngOnInit(): void {
    //generating random movies for display purposes
    this.generateRandomLetters();

    for (let index = 0; index < this.searchLetters.length; index++) {
      const letter = this.searchLetters[index];
      this.apiService
        .getMovies(letter)
        .pipe(take(1))
        .subscribe((data) => {
          this.filteredMovies.push(data);
          this.filteredMovies = this.filteredMovies.filter(
            (movie) => movie.Response
          );
        });
    }

    //subscribe to behaviour subject to get current theme
    this.colorSchemeService.isDark
      .pipe(takeUntil(this.$destroyed))
      .subscribe((isDark) => {
        this.isDark = isDark;
      });
  }

  handleSearch(searchData) {
    this.isSearching = true;
    if (searchData.title?.length === 1 || searchData.year?.length === 1) {
      this.filteredMovies = [];
    }
    this.year = searchData.year;
    this.title = searchData.title;

    this.apiService
      .getMovies(this.title, this.year)
      .pipe(take(1))
      .subscribe((data: any) => {
        //removing duplicates
        if (
          !this.filteredMovies.find((movie) => movie.imdbID === data.imdbID)
        ) {
          this.filteredMovies.push(data);
        }
      });
  }

  handleShuffle() {
    //generate a new set of random movies
    this.generateRandomLetters();
    this.isSearching = false;
    this.filteredMovies = [];
    for (let index = 0; index < this.searchLetters.length; index++) {
      const letter = this.searchLetters[index];
      this.apiService
        .getMovies(letter)
        .pipe(take(1))
        .subscribe((data) => {
          this.filteredMovies.push(data);
          this.filteredMovies = this.filteredMovies.filter(
            (movie) => movie.Response
          );
        });
    }
  }

  handleViewMovie(movie) {
    this.router.navigate([`movie/${movie.imdbID}`]);
  }

  generateRandomLetters() {
    this.searchLetters = [];
    let letter = '';
    for (let i = 0; i < this.alphanumeric.length; i++) {
      letter += this.alphanumeric.charAt(
        Math.floor(Math.random() * this.alphanumeric.length)
      );
      if (!this.searchLetters.includes(letter)) {
        this.searchLetters.push(letter);
        letter = '';
      }
    }
    this.searchLetters = this.searchLetters.sort(() => Math.random() - 0.5);
  }

  ngOnDestroy() {
    this.$destroyed.next();
    this.$destroyed.complete();
  }
}
