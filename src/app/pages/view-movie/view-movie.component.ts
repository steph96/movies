import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ApiService } from 'src/app/_services/api.service';
import { ColorSchemeService } from 'src/app/_services/color-scheme.service';

@Component({
  selector: 'app-view-movie',
  templateUrl: './view-movie.component.html',
  styleUrls: ['./view-movie.component.scss'],
})
export class ViewMovieComponent implements OnInit {
  public $destroyed: Subject<void> = new Subject();

  movieId: string;
  selectedMovie;
  genres: string[] = [];

  isDark: boolean = this.colorSchemeService.isDark.value;

  arSubs: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private router: Router,
    private colorSchemeService: ColorSchemeService
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(take(1)).subscribe((params) => {
      this.movieId = params['Id'];
    });

    this.apiService
      .getMovieById(this.movieId)
      .pipe(take(1))
      .subscribe((data) => {
        this.selectedMovie = data;
        this.genres = this.selectedMovie.Genre.split(',');
        this.genres = this.genres.filter((genre) => genre !== 'N/A');
      });

    this.colorSchemeService.isDark
      .pipe(takeUntil(this.$destroyed))
      .subscribe((isDark) => {
        this.isDark = isDark;
      });
  }

  handleBack() {
    this.router.navigate([`home`]);
  }

  ngOnDestroy() {
    this.$destroyed.next();
    this.$destroyed.complete();
    this.arSubs.forEach((sub) => sub.unsubscribe());
  }
}
